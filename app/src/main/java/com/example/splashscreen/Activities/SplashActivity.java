package com.example.splashscreen.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.splashscreen.R;
import com.example.splashscreen.Utilities;

public class SplashActivity extends AppCompatActivity {


    private final int SPLASH_DISPLAY_LENGTH = 1000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                String status = Utilities.getString(SplashActivity.this,"Login_status");

                if (status.equals("true")){

                    Intent intent = (new Intent(SplashActivity.this,UserInfoActivity.class));
                    intent.putExtra("from","login");
                    startActivity(intent);
                }else {

                    startActivity(new Intent(SplashActivity.this,MainActivity.class));
                    finish();

                }


            }
        }, SPLASH_DISPLAY_LENGTH);

    }
}