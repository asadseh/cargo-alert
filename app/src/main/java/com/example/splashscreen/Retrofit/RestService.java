package com.example.splashscreen.Retrofit;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

/**
 * Created by apple on 12/18/17.
 */

public interface RestService {

    @FormUrlEncoded
    @POST("userRegister")
    Call<ResponseBody> Signup(

            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String passowrd,
            @HeaderMap Map<String, String> headers

    );


    @FormUrlEncoded
    @POST("userLogin")
    Call<ResponseBody> login(

            @Field("email") String email,
            @Field("password") String password,
            @HeaderMap Map<String, String> headers

    );


    @FormUrlEncoded
    @POST("update_profile")
    Call<ResponseBody> profileupdate(

            @Field("user_id") String user_id,
            @Field("image") String image,
            @Field("email") String email,
            @Field("phone") String phone,
            @Field("dob") String dob,
            @Field("address") String address,
            @HeaderMap Map<String, String> headers

    );


    @FormUrlEncoded
    @POST("my_health_record")
    Call<ResponseBody> helthrecord(

            @Field("user_id") String user_id,
            @Field("date_recorded") String date_recorded,
            @Field("gender") String gender,
            @Field("age") String age,
            @Field("weight") String weight,
            @Field("bmi") String bmi,
            @Field("blood_pressure") String blood_pressure,
            @Field("fasting_cholestrol") String fasting_cholestrol,
            @Field("fasting_blood_glucose") String fasting_blood_glucose,
            @HeaderMap Map<String, String> headers

    );


    @FormUrlEncoded
    @POST("my_measurements")
    Call<ResponseBody> postmeasurments(

            @Field("user_id") String user_id,
            @Field("waist") String waist,
            @Field("hips") String hips,
            @Field("chest") String chest,
            @Field("thigh") String thigh,
            @Field("arm") String arm,
            @HeaderMap Map<String, String> headers

    );


}
