package com.example.splashscreen.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.splashscreen.R;
import com.example.splashscreen.Retrofit.RestService;
import com.example.splashscreen.Retrofit.UrlController;
import com.example.splashscreen.Utilities;
import com.gmail.samehadar.iosdialog.IOSDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {


    TextView login;
    EditText name,email,password;
    Button signup;

    Context context;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);


        init();



        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(SignUpActivity.this,LoginActivity.class));
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String namee = name.getText().toString();
                String emailll = email.getText().toString();
                String pass = password.getText().toString();



                if (namee.isEmpty()){

                    Toast.makeText(context, "Name is empty", Toast.LENGTH_SHORT).show();


                }else if (emailll.isEmpty()){

                    Toast.makeText(context, "Email is empty or invalid", Toast.LENGTH_SHORT).show();


                }else if (pass.isEmpty()){

                    Toast.makeText(context, "Password is empty", Toast.LENGTH_SHORT).show();


                }else {

                    Utilities.saveString(SignUpActivity.this,"name",namee);
                    Utilities.saveString(SignUpActivity.this,"email",emailll);
                    Utilities.saveString(SignUpActivity.this,"password",pass);

                    Toast.makeText(context, "Registered successfully", Toast.LENGTH_SHORT).show();
                    Utilities.saveString(context,"Login_status","true");


                    Intent intent = (new Intent(SignUpActivity.this,UserInfoActivity.class));
                    intent.putExtra("from","signup");
                    startActivity(intent);

//                    Signup(namee,emailll,pass);
                }


            }
        });

    }

    private void init() {

        context = SignUpActivity.this;

        login = findViewById(R.id.login);
        name = findViewById(R.id.name_et);
        email = findViewById(R.id.email_et);
        password = findViewById(R.id.pass_et);
        signup = findViewById(R.id.btn_signup);
    }

    private void Signup(String namee, String emailll, String pass) {



        final IOSDialog dialog0 = new IOSDialog.Builder(context)
                .setTitleColorRes(R.color.gray)
                .build();
        dialog0.show();

        RestService restService = UrlController.createService(RestService.class);

        Call<ResponseBody> myCall = restService.Signup( namee,emailll,pass,
                UrlController.AddHeaders(context));
        myCall.enqueue(new Callback<ResponseBody>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> responseObj) {
                try {

                    if (responseObj.isSuccessful()) {

                        dialog0.dismiss();

                        JSONObject response = new JSONObject(responseObj.body().string());

                        boolean status = response.getBoolean("status");
                        if (status){

                            String message = response.getString("message");

                            dialog0.dismiss();

                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                            Intent mainIntent = new Intent(context, LoginActivity.class);
                            startActivity(mainIntent);
                            finish();
                        }

                    }else {


                        dialog0.dismiss();
                        Toast.makeText(context,"Something went wrong", Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {

                    dialog0.dismiss();

                    e.printStackTrace();
                } catch (IOException e) {

                    dialog0.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                dialog0.dismiss();

                Log.d("info Login error", String.valueOf(t));
                Log.d("info Login error", String.valueOf(t.getMessage() + t.getCause() + t.fillInStackTrace()));
            }
        });





    }

}