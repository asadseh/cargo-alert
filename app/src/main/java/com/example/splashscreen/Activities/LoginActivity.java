package com.example.splashscreen.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.splashscreen.R;
import com.example.splashscreen.Retrofit.RestService;
import com.example.splashscreen.Retrofit.UrlController;
import com.example.splashscreen.Utilities;
import com.gmail.samehadar.iosdialog.IOSDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    TextView signup,forget_pass;
    Button login;
    EditText email,password;

    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();



    }

    private void init() {

        context = LoginActivity.this;

        email = findViewById(R.id.email_et);
        password = findViewById(R.id.pass_et);
        login = findViewById(R.id.btn_login);
        signup = findViewById(R.id.signup);



        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(LoginActivity.this,SignUpActivity.class));
            }
        });



        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             Login();

            }
        });

    }

    private void Login() {

        String email_st = email.getText().toString();
        String password_st = password.getText().toString();

        if (email_st.isEmpty()){

            Toast.makeText(context, "Email is empty or invalid", Toast.LENGTH_SHORT).show();


        }else if (password_st.isEmpty()){

            Toast.makeText(context, "Password is empty ", Toast.LENGTH_SHORT).show();


        }else {


            String emaill = Utilities.getString(LoginActivity.this,"email");

            if (emaill.equals(email_st)){

                if (password_st.equals(Utilities.getString(LoginActivity.this,"password"))){

                    Utilities.saveString(context,"Login_status","true");
                    Utilities.saveString(context,"logged_in_email",email_st);

                    Toast.makeText(context, "Logged in", Toast.LENGTH_SHORT).show();

                    Intent intent = (new Intent(LoginActivity.this,UserInfoActivity.class));
                    intent.putExtra("from","login");
                    startActivity(intent);

                    finish();

                }else {

                    Toast.makeText(context, "Wrong Password", Toast.LENGTH_SHORT).show();

                }


            }else {

                Toast.makeText(context, "Sorry you can't Loggin", Toast.LENGTH_SHORT).show();

            }


//            ProceedLogin(email_st,password_st);
        }


    }

    private void ProceedLogin(String email_st, String password_st) {

        final IOSDialog dialog0 = new IOSDialog.Builder(context)
                .setTitleColorRes(R.color.gray)
                .build();
        dialog0.show();



        RestService restService = UrlController.createService(RestService.class);

        Call<ResponseBody> myCall = restService.login( email_st, password_st,
                UrlController.AddHeaders(LoginActivity.this));
        myCall.enqueue(new Callback<ResponseBody>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> responseObj) {
                try {

                    if (responseObj.isSuccessful()) {

                        dialog0.dismiss();

                        JSONObject response = new JSONObject(responseObj.body().string());

                        boolean status = response.getBoolean("status");
                        if (status){

                            dialog0.dismiss();
                            String message = response.getString("message");


                            JSONObject jsonObject1 = response.getJSONObject("user");

                            int id = jsonObject1.getInt("id");
                            String name = jsonObject1.getString("name");
                            String email = jsonObject1.getString("email");

                            Utilities.saveInt(context,"user_id",id);
                            Utilities.saveString(context,"user_name",name);
                            Utilities.saveString(context,"user_email",email);

                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                            Intent mainIntent = new Intent(context, UserInfoActivity.class);
                            startActivity(mainIntent);
                            finish();
                        }



                    }else {

                        dialog0.dismiss();
                        Toast.makeText(LoginActivity.this,"Something went wrong", Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {

                    dialog0.dismiss();

                    e.printStackTrace();
                } catch (IOException e) {

                    dialog0.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                dialog0.dismiss();
                Log.d("info Login error", String.valueOf(t));
                Log.d("info Login error", String.valueOf(t.getMessage() + t.getCause() + t.fillInStackTrace()));
            }
        });





    }
}