package com.example.splashscreen.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.example.splashscreen.R;

import java.util.ArrayList;

public class ViewPager_Adpter extends PagerAdapter {

    Context context;
    ArrayList<Integer> images;

    public ViewPager_Adpter(Context context, ArrayList<Integer> images) {
        this.context = context;
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {

        // pass object in view
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        //add layout
        // add wediget ....Imageview
        //add in container
        // return view

        View view = LayoutInflater.from(context).inflate(R.layout.item_viewpager , null );
        ImageView  imageView = (ImageView) view.findViewById(R.id.imageview);
        Glide.with(context).asBitmap().load(images.get(position)).into(imageView);
        container.addView(view,0);
        return  view;
     }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //remove object from conatiner
       container.removeView((View) object);
    }
}
