package com.example.splashscreen.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.splashscreen.R;
import com.example.splashscreen.Adapter.ViewPager_Adpter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ViewPager viewPager;
    ArrayList<Integer> images;
    ViewPager_Adpter adpter;

    private ImageView nextbutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        nextbutton = (ImageView) findViewById(R.id.button_next);
        images = new ArrayList<>();

        images.add(R.drawable.image1);
        images.add(R.drawable.image2);

        adpter = new ViewPager_Adpter(this, images);
        viewPager.setPadding(0, 0, 0, 0);
        viewPager.setAdapter(adpter);


        nextbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this,LoginActivity.class));
                finish();

            }
        });
    }
}