package com.example.splashscreen.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.splashscreen.R;

public class FinalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final);
    }
}