package com.example.splashscreen.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.splashscreen.MyBroadcastReceiver;
import com.example.splashscreen.R;
import com.example.splashscreen.Utilities;

import java.util.Calendar;
import java.util.Date;

public class UserInfoActivity extends AppCompatActivity {

    AlarmManager  manager;
    PendingIntent pendingIntent;

    TimePicker myTimePicker;
    TimePickerDialog timePickerDialog;
    EditText time;
    Button btn_go;

    final static int RQS_1 = 1;


    TimePicker timePicker;
    Button setalarm;
    int Hour,Mint;


    String type = "",object = "";
    LinearLayout home_lin;
    CardView alarm_card;

    ImageView del;
    TextView alarm_name,alarm_object;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        btn_go = findViewById(R.id.btn_go);
        timePicker = (TimePicker) findViewById(R.id.timepicker);
        alarm_card =  findViewById(R.id.alarm_card);
        home_lin =  findViewById(R.id.home_lin);
        alarm_name =  findViewById(R.id.name);
        alarm_object = findViewById(R.id.Object);
        del =  findViewById(R.id.del);


        if (Utilities.getString(UserInfoActivity.this,"alarm_status").equals("true")){

            alarm_card.setVisibility(View.VISIBLE);
            home_lin.setVisibility(View.GONE);

            alarm_name.setText(Utilities.getString(UserInfoActivity.this,"type"));
            alarm_object.setText("Alarm for  "+Utilities.getString(UserInfoActivity.this,"object")+" is already settled");


        }else {

            alarm_card.setVisibility(View.GONE);
            home_lin.setVisibility(View.VISIBLE);
        }


        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
        final Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);





        String from = getIntent().getStringExtra("from");
        if (from.equals("notification")){

            if (MyBroadcastReceiver.ringtone != null){

                MyBroadcastReceiver.ringtone.stop();
                MyBroadcastReceiver.vibrator.cancel();


            }
//            if (ringtone != null) ringtone.stop();

//            Ringtone ringtone = RingtoneManager.getRingtone(UserInfoActivity.this,noti);
//            ringtone.stop();

        }



        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//              startActivity(new Intent(UserInfoActivity.this, FinalActivity.class));

                type = spinner.getSelectedItem().toString();
                object = spinner2.getSelectedItem().toString();


                Utilities.saveString(UserInfoActivity.this,"type",type);
                Utilities.saveString(UserInfoActivity.this,"object",object);

                if (type.equals("One Time Alarm")){

                    setOne_timealarm();

                }else {

                    setDailyalarm();
                }


            }
        });

        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int hourOfday, int minute) {

                Hour = hourOfday;
                Mint = minute;
//                timeofalarm.setText(timeofalarm.getText().toString()+ " "+Hour+ ":" +Mint);
            }
        });

        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deleteAlarm();
            }
        });


    }

    private void deleteAlarm() {

        manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);

        Intent i = new Intent(UserInfoActivity.this, MyBroadcastReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(UserInfoActivity.this,24444,i,0);
        manager.cancel(pendingIntent);

        Utilities.saveString(UserInfoActivity.this,"alarm_status","false");
        alarm_card.setVisibility(View.GONE);
        home_lin.setVisibility(View.VISIBLE);


    }

    private void setDailyalarm() {

        manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Date date = new Date();


        Calendar alarm = Calendar.getInstance();
        Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        alarm.setTime(date);

        alarm.set(Calendar.HOUR_OF_DAY,Hour);
        alarm.set(Calendar.MINUTE,Mint);
        alarm.set(Calendar.SECOND,0);
        long startUpTime = cal.getTimeInMillis();



        if(alarm.before(cal)){

            alarm.add(Calendar.DATE,1);

        }

        Intent i = new Intent(UserInfoActivity.this, MyBroadcastReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(UserInfoActivity.this,24444,i,0);
        manager.set(AlarmManager.RTC_WAKEUP,alarm.getTimeInMillis(),pendingIntent);
        manager.setRepeating(AlarmManager.RTC_WAKEUP,
                startUpTime, AlarmManager.INTERVAL_DAY, pendingIntent);

        Utilities.saveString(UserInfoActivity.this,"alarm_status","true");



        startActivity(new Intent(UserInfoActivity.this,FinalActivity.class));
        finish();
    }

    private void setOne_timealarm() {

        AlarmManager  manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Date date = new Date();


        Calendar alarm = Calendar.getInstance();
        Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        alarm.setTime(date);

        alarm.set(Calendar.HOUR_OF_DAY,Hour);
        alarm.set(Calendar.MINUTE,Mint);
        alarm.set(Calendar.SECOND,0);



        if(alarm.before(cal)){

            alarm.add(Calendar.DATE,1);

        }

        Intent i = new Intent(UserInfoActivity.this, MyBroadcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(UserInfoActivity.this,24444,i,0);
        manager.set(AlarmManager.RTC_WAKEUP,alarm.getTimeInMillis(),pendingIntent);


        Utilities.saveString(UserInfoActivity.this,"alarm_status","true");

        startActivity(new Intent(UserInfoActivity.this,FinalActivity.class));
        finish();
    }


}